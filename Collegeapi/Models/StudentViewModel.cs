﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collegeapi.Models
{
    public class StudentViewModel
    {
        public int Studentid { get; set; }

        public string Studentname { get; set; }
        public string Stu_address { get; set; }

        public string Stu_phone { get; set; }

        public string Stu_email { get; set; }


    }
}