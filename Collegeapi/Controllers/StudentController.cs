﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Collegeapi.Models;


namespace Collegeapi.Controllers
{
    public class StudentController : ApiController
    {
        //GET- Retrive data

        public IHttpActionResult GetAllStudent()
        {
            IList<StudentViewModel> students = null;
            using (var y = new CollegeEntities())
            {
                students = y.Students
                    .Select(c => new StudentViewModel()
                    {
                        Studentid = c.Studentid,
                        Studentname = c.Studentname,
                        Stu_address = c.Stu_address,
                        Stu_phone = c.Stu_phone,
                        Stu_email = c.Stu_email

                    }).ToList<StudentViewModel>();
            }
            if (students.Count == 0)
                return NotFound();
            return Ok(students);

        }
        //Post-insert new record
        public IHttpActionResult PostNewStudent(StudentViewModel student)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.please return");

   
              using (var y = new CollegeEntities())
            {
                y.Students.Add(new Student()
                {
                   Studentid = student.Studentid,
                    Studentname = student.Studentname,
                    Stu_address = student.Stu_address,
                    Stu_phone = student.Stu_phone,
                    Stu_email = student.Stu_email

                });
                y.SaveChanges();

            }
            return Ok();
        }
       
        //Put-Update data
        public IHttpActionResult PutStudent(StudentViewModel student)
        {
            if (!ModelState.IsValid)
                return BadRequest("please ivlid model,please check");
            using(var y=new CollegeEntities())
            {
                var checkExsitingStudent=y.Students.Where(c =>c.Studentid ==student.Studentid).FirstOrDefault<Student>();
                if (checkExsitingStudent != null)
                {
                    checkExsitingStudent.Studentname = student.Studentname;
                    checkExsitingStudent.Stu_address = student.Stu_address;
                    checkExsitingStudent.Stu_phone = student.Stu_phone;
                    checkExsitingStudent.Stu_email = student.Stu_email;

                    y.SaveChanges();

                }
                else
                    return NotFound();

            }
            return Ok();

        }

        //Delete the record from database

        public IHttpActionResult Delete(int Studentid)
        {
            if (Studentid <= 0)
                return BadRequest("please enter correct studnt id");

            using (var y=new CollegeEntities())
            {
                var student = y.Students.Where(c => c.Studentid == Studentid)
                    .FirstOrDefault();
                y.Entry(student).State = System.Data.Entity.EntityState.Deleted;
                y.SaveChanges();

            }
            return Ok();
        }
    }

}
